extends KinematicBody2D

var _shutUPWARNINGS = null

var MAXHEALTH = 1
var TYPE = "ENEMY"
export(int) var SPEED = 0

var movedir = Vector2(0, 0)
var knockdir = Vector2(0, 0)
var spritedir = "down" # Sprite Direction - what way is the player facing by default?

export(int) var health = MAXHEALTH
var hitstun = 0
var texture_default = null
var texture_hurt    = null

func _ready():
	if TYPE == "ENEMY":
		set_collision_mask_bit(1,1)
		set_physics_process(false) # disable enemy movement, camera scene has a connect that reenables it on enery
	#texture_default = $Sprite.texture
	#texture_hurt = load($Sprite.texture.get_path().replace(".png", "_hurt.png"))

func movement_loop():
	var motion
	if hitstun == 0:
		motion = movedir.normalized() * SPEED
	else:
		motion = knockdir.normalized() * 125
	_shutUPWARNINGS = move_and_slide(motion, Vector2(0,0)) # Godot needs to be told what direction the char is, 0,0 is used for top down games

func spritedir_loop():
	match movedir:
		Vector2(-1,0):
			spritedir = "left"
		Vector2(1,0):
			spritedir = "right"
		Vector2(0,-1):
			spritedir = "up"
		Vector2(0,1):
			spritedir = "down"

func anim_switch(animation):
	var _shutUPWARNINGS2 = animation
	var newanim = str(animation, spritedir)
	if $anim.current_animation != newanim:
		$anim.play(newanim)

func damage_loop():
	health = min(MAXHEALTH, health)
	if hitstun > 0:
		hitstun -= 1
		$Sprite.texture = texture_hurt
	else:
		$Sprite.texture = texture_default
		if TYPE == "ENEMY" && health <= 0:
			var drop = randi() % 3
			if drop == 0:
				#instance_scene(preload("res://pickups/heart.tscn"))
			#instance_scene(preload("res://enemies/enemy_death.tscn"))
			queue_free()
	for area in $hitbox.get_overlapping_areas():
		var body = area.get_parent()
		# if not hit yet (=0) AND entity deals damage (has a damage value) AND the type is NOT the same as player type
		if hitstun == 0 and body.get("DAMAGE") != null and body.get("TYPE") != TYPE:
			health -= body.get("DAMAGE")
			hitstun = 10
			knockdir = global_transform.origin - body.global_transform.origin

#func use_item(item):
#	var newitem = item.instance() # make an instance of the item scene
#	newitem.add_to_group(str(newitem.get_name(), self))
#	add_child(newitem)
#	# check for item max amount
#	if get_tree().get_nodes_in_group(str(item, self)).size() > newitem.maxamount:
#		newitem.queue_free() # remove new instance of item... 31 to 30 of arrows for example
		
func instance_scene(scene):
	# create, for example, a heart pickup instance
	var new_scene = scene.instance()
	# set to parent, the enemy that drops the heart
	new_scene.global_position = global_position
	get_parent().add_child(new_scene)
